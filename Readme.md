# WorldSigns
Let's face it - Multiverse-SignPortals *sucks*. For example, you can't have
something like this:  
    
Speed SW 1  
0/16 Players  
Waiting  
10+ Required  
    
Instead, you must have this:
   
[MV]  
SpeedSW1  
  
## Installation
1. Download it.
2. Put the Jar in the plugins folder.
3. Done.
