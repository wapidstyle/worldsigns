package wapidstyle.worldsigns;

import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;

public class WorldSignsPlugin extends JavaPlugin{
	ToggleListener tl = new ToggleListener();
	CreateWorldSign c = new CreateWorldSign();
	CommandSender s;
	
	@Override
	public void onEnable(){
		//getServer().getPluginManager().registerEvents(new BlockListener(), this);
		getLogger().info("Successfully loaded WorldSigns 1.0-SNAPSHOT!");
	}
	
	@Override
	public void onDisable(){
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args){
	    if(cmd.getName().equalsIgnoreCase("ws")){
	    	sender = s;
	    	c.stp1(s);
	    }
	       return false;
	}
	public CommandSender getSender(){
		return s;
	}
}
