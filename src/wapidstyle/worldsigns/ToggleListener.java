package wapidstyle.worldsigns;

public class ToggleListener {
	Toggle listener1 = Toggle.OFF;
	Toggle listener2 = Toggle.OFF;
	boolean returngt1 = false;
	boolean returngt2 = false;
	Return toreturn = Return.NULL;
	
	public void toggleListener(int l){
		if(l == 0){
			if(listener1 == Toggle.OFF){
				listener1 = Toggle.ON;
				returngt1 = true;
			}
		} else {
			if(listener2 == Toggle.OFF){
				listener2 = Toggle.ON;
				returngt2 = false;
			} else {
				listener2 = Toggle.OFF;
				returngt2 = false;
			}
		}
	}
	
	public boolean returnMode(int l){
		if(l == 0){
			toreturn = Return.ONE;
		} else {
			if(l == 1){
				toreturn = Return.TWO;
			} else {
				toreturn = Return.NULL;
			}
		}
		if(toreturn == Return.ONE || toreturn == Return.NULL){
			return returngt1;
		} else {
			return returngt2;
		}
	}
	public void setMode(int l, boolean m){
		if(l == 0){
			if(m == true){
				listener1 = Toggle.ON;
				returngt1 = true;
			} else {
				listener1 = Toggle.OFF;
				returngt1 = false;
			}
		} else {
			if(m == true){
				listener2 = Toggle.ON;
				returngt2 = true;
			} else {
				listener2 = Toggle.OFF;
				returngt2 = false;
			}
		}

	}
	
}
enum Toggle{
	ON, OFF
}
enum Return{
	NULL, ONE, TWO
}
