package wapidstyle.worldsigns;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CreateWorldSign {
	ToggleListener tl = new ToggleListener();
	public void console(CommandSender s){
		s.sendMessage(ChatColor.ITALIC + "You are not a player!");
	}
	public void stp1(CommandSender s){
		if(s instanceof Player){
			tl.setMode(0, false);
			String message0 = ChatColor.GOLD + "[" + ChatColor.YELLOW + "WorldSigns";
			message0 = message0 + ChatColor.GOLD + "]" + ChatColor.GREEN + " Click on a ";
			message0 = message0 + "sign to make it a WorldSign!";
			s.sendMessage(message0);
			tl.setMode(0, true);
		}
		if(!(s instanceof Player)){
			console(s);
		}
	}
    public void stp2(CommandSender s){
    	if(s instanceof Player){
    		tl.setMode(0, false);
    		String message1 = ChatColor.GOLD + "[" + ChatColor.YELLOW + "WorldSigns";
    		message1 = message1 + ChatColor.GOLD + "]" + ChatColor.DARK_GREEN + " Great!" + ChatColor.GREEN + " Click on a";
    		message1 = message1 + ChatColor.GREEN + " block's face to get the sign to rest";
    		message1 = message1 + ChatColor.GREEN + " there!";
    		tl.setMode(1, true);
    	}
	}
}
